# DTU/SPOC Poster Description

Latex class for creating posters DTU themede posters with SPOC and Villum fonden logos included and also adding support for the 700 mm x 1000 mm size.

This is a slightly modified and updated version of the version by
Jorrit Wronski (jowr@mek.dtu.dk) which can be found here:
https://gitlab.gbar.dtu.dk/latex/dtutemplates